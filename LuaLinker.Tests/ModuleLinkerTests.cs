﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LuaLinker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuaLinker.Tests
{
    [TestClass]
    public class ModuleLinkerTests
    {

        [TestMethod]
        public void MergeTest()
        {
            var mainModule = new LuaModule("main",
@"local api = require('api')
local test = require('test')

function main()
    print('ok')
end");
            var apiModule = new LuaModule("api",
@"local test = require('test')
local api = {}

function api.call()
    print('api')
end

return api");
            var testModule = new LuaModule("test", @"nothing");
            var allModules = new[]
            {
                mainModule,apiModule,testModule
            };
            var linker = new ModuleLinker();
            var merged = linker.Merge(mainModule, allModules);


        }
    }
}