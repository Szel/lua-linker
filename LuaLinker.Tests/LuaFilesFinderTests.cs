﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LuaLinker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace LuaLinker.Tests
{
    [TestClass]
    public class LuaFilesFinderTests
    {
        private string testFolder = Path.Combine(Directory.GetCurrentDirectory(), "filesFinderTests/");

        [TestInitialize]
        public void Setup()
        {
            Directory.CreateDirectory(testFolder);
            var files = new[]
            {
                Path.Combine(testFolder, "notimportantfile.md"),
                Path.Combine(testFolder, "main.lua"),
                Path.Combine(testFolder, "something.lua"),
                Path.Combine(testFolder, "folder/infolder1.lua"),
                Path.Combine(testFolder, "folder2/infolder2.lua"),
                Path.Combine(testFolder, "folder2/secondinfolder2.lua"),
            };

            foreach (var file in files)
            {
                if (!Directory.Exists(Path.GetDirectoryName(file)))
                    Directory.CreateDirectory(Path.GetDirectoryName(file));

                using (File.Create(file, 1)) { }
            }
        }

        [TestCleanup]
        public void CleanUp()
        {
            Directory.Delete(testFolder, true);
        }

        [TestMethod]
        public void GetFiles_finds_all_files_test()
        {
            var filesFinder = new LuaFilesFinder(testFolder);
            var files = filesFinder.GetFiles();

            Assert.AreEqual(5, files.Count());
        }

        [TestMethod]
        public void GetFiles_correct_mapping_test()
        {
            var filesFinder = new LuaFilesFinder(testFolder);
            var files = filesFinder.GetFiles();

            var rootFile = files.FirstOrDefault(f => f.FileName == "main.lua");
            var nestedFile = files.FirstOrDefault(f => f.FileName == "infolder2.lua");

            Assert.IsNotNull(rootFile);
            Assert.IsNotNull(nestedFile);
            Assert.AreEqual("main", rootFile.FullName);
            Assert.AreEqual("folder2/infolder2", nestedFile.FullName);

        }

        [TestMethod]
        public void GetFile_short_path_test()
        {
            var ff = new LuaFilesFinder("filesFinderTests/main.lua");

            var module = ff.GetMainModule();
            Assert.AreEqual("main", module.FullName);
        }
    }
}