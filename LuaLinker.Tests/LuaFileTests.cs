﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using LuaLinker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LuaLinker.Tests
{
    [TestClass]
    public class LuaFileTests
    {
        [TestMethod]
        public void RequiredModules_return_full_module_path_test()
        {
            var file = new LuaModule("path/modulle",
@"m1 = require ""module1""
m2 = require('module2')
m3 = require'path/to/module'
temp = 333
print(123)
");
            var modules = file.RequiredModules.Select(m => m.ModuleFullName);
            var expectedModules = new[]
            {
                "path/module1", "path/module2", "path/path/to/module"
            };

            CollectionAssert.AreEqual(expectedModules, modules.ToArray());
        }

        [TestMethod]
        public void GetModuleNameTest()
        {
            var file = new LuaModule("module", "");

            var file2 = new LuaModule("path/to/module", "");

            Assert.AreEqual("module", file.Name);
            Assert.AreEqual("module", file2.Name);
        }

        [TestMethod]
        public void GetModuleName_full_path_is_null_test()
        {
            var file = new LuaModule(null, "");
            Assert.AreEqual("", file.Name);
        }

        [TestMethod]
        public void GetModulePathTest()
        {
            var file = new LuaModule("module", "");

            var file2 = new LuaModule("path/to/module", "");

            Assert.AreEqual("", file.Path);
            Assert.AreEqual("path/to", file2.Path);
        }

        [TestMethod]
        public void GetModulePath_full_path_is_null()
        {
            var file = new LuaModule(null, "");
            Assert.AreEqual("", file.Path);
        }
    }
}