﻿using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace LuaLinker
{
    public class LuaModule
    {
        public LuaModule(string fullName, string content)
        {
            FullName = fullName;
            Content = ParseContent(content);
        }

        public string FullName { get; }
        public string Name
        {
            get
            {
                if (FullName == null)
                    return "";
                return FullName.Split('/').Last();
            }
        }

        public string Path
        {
            get
            {
                if (FullName == null)
                    return "";
                var fragments = FullName.Split('/');
                return string.Join("/", fragments.Take(fragments.Length - 1));
            }
        }

        public string FileName { get; set; }
        public string Content { get; }

        public List<Require> RequiredModules { get; private set; } = new List<Require>();

        private string ParseContent(string value)
        {
            var modules = new List<Require>();
            var requireRegex = new Regex(
                "([^=\\s]+)" + // variable
                "\\s*=\\s*" + // equals
                "require\\s*" + // require
                "\\(?\\s*[\"']" +
                "([^\"']+)" + // module name
                "[\"']\\s*\\)?");
            var path = string.IsNullOrEmpty(Path) ? "" : (Path + "/");

            var parsedContent = new StringBuilder();
            using (var stringReader = new StringReader(value))
            {
                while (true)
                {
                    var line = stringReader.ReadLine();
                    if (line == null)
                        break;

                    var normalizedLine = line; // NormalizeLine(line);
                    var match = requireRegex.Match(normalizedLine);
                    if (!match.Success)
                    {
                        parsedContent.AppendLine(line);
                        parsedContent.AppendLine(stringReader.ReadToEnd());
                        break;
                    }

                    var variableName = match.Groups[1].Value;
                    var require = new Require()
                    {
                        ModuleFullName = path + match.Groups[2].Value
                    };
                    modules.Add(require);
                    line = requireRegex.Replace(normalizedLine,
                        string.Format("{0} = LINKER_MODULES.get('{1}')", variableName, require.ModuleFullName));
                    parsedContent.AppendLine(line);
                }
            }

            RequiredModules = modules;
            return parsedContent.ToString();
        }


        private string NormalizeLine(string line)
        { 
            return Regex.Replace(line, @"([^a-zA-Z])\s+([^a-zA-Z])", "");
        }
    }
}