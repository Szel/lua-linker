﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace LuaLinker
{

    public class LuaFilesFinder
    {
        private const string LuaExtension = ".lua";
        private string workDirectory;
        private string mainFilePath;

        public LuaFilesFinder(string mainFilePath)
        {
            this.mainFilePath = Path.GetFullPath(mainFilePath);
            this.workDirectory = Path.GetFullPath(Path.GetDirectoryName(mainFilePath));
        }

        public LuaModule GetMainModule()
        {
            return new LuaModule(GetModuleName(mainFilePath), FileContent(mainFilePath));
        }

        public IEnumerable<LuaModule> GetFiles()
        {
            var allLuaFiles = Directory.GetFiles(workDirectory, "*" + LuaExtension, SearchOption.AllDirectories);
            var files = new List<LuaModule>();
            foreach (var luaFile in allLuaFiles)
            {
                files.Add(new LuaModule(GetModuleName(luaFile), FileContent(luaFile))
                {
                    FileName = Path.GetFileName(luaFile)
                });
            }

            return files;
        }

        private string GetModuleName(string luaFile)
        {
            var moduleName = luaFile.Substring(workDirectory.Length + 1);
            moduleName = moduleName.Remove(moduleName.Length - LuaExtension.Length)
                .Replace(Path.DirectorySeparatorChar, '/');

            return moduleName;
        }

        private string FileContent(string path)
        {
            for (int i = 0; i < 5; i++)
            {
                try
                {
                    return File.ReadAllText(path);
                }
                catch (IOException ex)
                {
                    Console.WriteLine(ex.Message);
                    Thread.Sleep(TimeSpan.FromSeconds(2));
                }
            }
            throw new IOException("Couldn't load file");
        }
    }
}