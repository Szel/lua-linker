﻿using log4net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;

namespace LuaLinker
{
    class Linker
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string mainFile;
        private IEnumerable<string> modules;
        private string joinedFiles;
        public string ProjectPath { get; set; }
        public string OutputFilename { get; set; }

        public Linker(string projectPath)
        {
            ProjectPath = projectPath;
        }

        public bool FindFiles()
        {
            var mainFileSuffix = "main.lua";
            var orderedSuffixPattern = @"\.([0-9]+)\.lua$";
            var moduleSuffix = ".part.lua";


            if (!Directory.Exists(ProjectPath))
            {
                log.Error($"Directory not found: {ProjectPath}");
                return false;
            }

            var allFiles = Directory.GetFiles(ProjectPath);
            mainFile = allFiles.FirstOrDefault(f => f.EndsWith(mainFileSuffix));
            if (mainFile == null)
            {
                log.Error($"Cannot find main file (should end with \"{mainFileSuffix}\")");
                return false;
            }

            modules = allFiles
                .Where(f => Regex.IsMatch(f, orderedSuffixPattern))
                .OrderBy(f => Regex.Match(f, orderedSuffixPattern).Groups[1].Value)
                .ToList();

            (modules as List<string>).AddRange(allFiles.Where(f => f.EndsWith(moduleSuffix)));

            return true;
        }

        public void JoinFiles()
        {
            var result = "";
            if (modules != null)
            {
                foreach (var module in modules)
                {
                    if (result != string.Empty)
                        result += Environment.NewLine;
                    var i = 0;
                    do
                    {
                        try
                        {
                            result += "-- " +
                                Path.GetFileName(module) +
                                Environment.NewLine +
                                File.ReadAllText(module);
                            break;
                        }
                        catch (IOException ex)
                        {
                            log.Error(ex.Message);
                            Thread.Sleep(500);
                            i++;
                        }
                    } while (i < 3);
                }
            }
            joinedFiles = result + 
                Environment.NewLine +
                "-- " +
                Path.GetFileName(mainFile) + 
                Environment.NewLine +
                File.ReadAllText(mainFile);
        }

        public void SaveResult()
        {
            var outputFileName = OutputFilename;
            if (string.IsNullOrEmpty(outputFileName))
            {
                outputFileName = mainFile.Replace("main.lua", "") + "output.lua";
            }

            File.WriteAllText(Path.Combine(ProjectPath, outputFileName), joinedFiles);
        }
    }
}
