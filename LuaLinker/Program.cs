﻿using log4net;
using NDesk.Options;
using System;
using System.IO;

namespace LuaLinker
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public static string mainModulePath;
        private static string workDirPath;
        private static string outputFilePath;
        private static LuaFilesFinder fileFinder;

        static void Main(string[] args)
        {
            bool watch = false;

            var options = new OptionSet()
            {
                {"m|mainpath=", "Path to main module.", (string v) => mainModulePath=v},
                {"o|output=", "Path of output file.", (string v) => outputFilePath=v},
                {"w|watch", "Program watches directory and runs when file was changed/created etc.", v=> { if (v != null) watch = true; } }
            };
            options.Parse(args);
            fileFinder = new LuaFilesFinder(mainModulePath);
            workDirPath = Path.GetDirectoryName(mainModulePath);
            RunLinker();
            if (watch)
            {
                Console.WriteLine($"Watching files in directory `{workDirPath}`\nPress `q` to exit.");
                var watcher = new FileSystemWatcher(workDirPath);
                watcher.Filter = "*.lua";
                watcher.Created += Watcher_Changed;
                watcher.Changed += Watcher_Changed;
                watcher.Deleted += Watcher_Changed;
                watcher.Renamed += Watcher_Renamed;
                watcher.EnableRaisingEvents = true;
                while (Console.ReadKey().Key != ConsoleKey.Q) { }
                watcher.Dispose();
            }


        }

        private static void Watcher_Renamed(object sender, RenamedEventArgs e)
        {
            Watcher_Changed(sender, new FileSystemEventArgs(WatcherChangeTypes.Changed, Path.GetDirectoryName(e.FullPath), e.Name));
        }

        private static void Watcher_Changed(object sender, FileSystemEventArgs e)
        {
            if (!e.Name.EndsWith("output.lua"))
            {
                log.Info($"Detected change in `{e.Name}`");
                RunLinker();
            }
        }


        private static void RunLinker()
        {
            var l = new ModuleLinker();
            var merged = l.Merge(fileFinder.GetMainModule(), fileFinder.GetFiles());
            File.WriteAllText(outputFilePath, merged);
        }

    }
}
