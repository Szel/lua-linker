﻿using System.Linq;
using System;
using System.Collections.Generic;
using System.Text;

namespace LuaLinker
{
    public class ModuleLinker
    {

        public string Merge(LuaModule mainFile, IEnumerable<LuaModule> files)
        {
            var requires = new List<Require>();
            GetRequired(mainFile, files, requires);

            var modulesToInsert = requires.Select(r => r.ModuleFullName).Distinct();
            var modulesMerged = new StringBuilder(
@"local LINKER_MODULES = {}
LINKER_MODULES.module= {}
LINKER_MODULES.loadedModules = {}
function LINKER_MODULES.get(name)
    if LINKER_MODULES.loadedModules[name] == nil then
        local module = LINKER_MODULES.module[name]()
        
        LINKER_MODULES.loadedModules[name] = module
        return module
    else
        return LINKER_MODULES.loadedModules[name]
    end
end
");
            foreach (var module in modulesToInsert)
            {
                modulesMerged.AppendLine(
                    string.Format(
                        "LINKER_MODULES.module['{0}'] = function() {2}{1}{2}end",
                        module,
                        files.First(f => f.FullName == module).Content,
                        Environment.NewLine));
            }
            modulesMerged.AppendLine(mainFile.Content);
            return modulesMerged.ToString();
        }

        private void GetRequired(LuaModule file, IEnumerable<LuaModule> allFiles, List<Require> requires)
        {
            var moduleRequired = file.RequiredModules;
            if (moduleRequired.Count == 0)
            {
                return;
            }
            requires.AddRange(moduleRequired);
            foreach (var require in moduleRequired)
            {
                GetRequired(allFiles.First(f => f.FullName == require.ModuleFullName), allFiles, requires);
            }
        }
    }
}
